ScoreBert is a scoreboard for your server. 
Each member gets 10 points each month to give to other members whenever they do something cool. 
Compete with your friends to see who will rank highest on the scoreboard!

Add ScoreBert to your server: https://discordapp.com/oauth2/authorize?client_id=364186658960048139&scope=bot

Discord and all related names, trademarks, and logos are owned by Discord Inc. | This program was written by Mitchell Augustin and is licensed under the Apache License version 2.0
https://www.apache.org/licenses/LICENSE-2.0.html